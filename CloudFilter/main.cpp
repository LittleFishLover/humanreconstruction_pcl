#include <iostream>
#include <fstream>

#include <pcl/point_types.h>
#include <pcl/filters/passthrough.h>
#include <pcl/io/pcd_io.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/visualization/pcl_visualizer.h>

using namespace std;
using pcl::visualization::PointCloudColorHandlerGenericField;
using pcl::visualization::PointCloudColorHandlerCustom;
//convenient typedefs
typedef pcl::PointXYZ PointT;
typedef pcl::PointCloud<PointT> PointCloud;


struct PCD
{
    PointCloud::Ptr cloud;
    std::string f_name;

    PCD() : cloud (new PointCloud) {};
};
void loadData (int argc, char **argv, std::vector<PCD, Eigen::aligned_allocator<PCD> > &models)
{
    std::string extension (".pcd");
    // Suppose the first argument is the actual test model
    for (int i = 1; i < argc; i++)
    {
        std::string fname = std::string (argv[i]);
        // Needs to be at least 5: .plot
        if (fname.size () <= extension.size ())
            continue;

        std::transform (fname.begin (), fname.end (), fname.begin (), (int(*)(int))tolower);

        //check that the argument is a pcd file
        if (fname.compare (fname.size () - extension.size (), extension.size (), extension) == 0)
        {
            // Load the cloud and saves it into the global list of models
            PCD m;
            m.f_name = argv[i];
            pcl::io::loadPCDFile (argv[i], *m.cloud);
            //remove NAN points from the cloud
            std::vector<int> indices;
            pcl::removeNaNFromPointCloud(*m.cloud,*m.cloud, indices);

            models.push_back (m);
        }
    }
}


int
main (int argc, char** argv)
{
/*
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered1 (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_filtered2 (new pcl::PointCloud<pcl::PointXYZ>);
    //load a pcd file
    std::string pcdfile;
    cout<<"please input filename:\n"<<endl;
    cin>>pcdfile;
    pcl::io::loadPCDFile(pcdfile,*cloud);
    if (pcl::io::loadPCDFile<pcl::PointXYZ> (pcdfile, *cloud) == -1) // load the file
    {
        PCL_ERROR ("Couldn't read pcd file ! \n");
        return (-1);
    }
    else{
        cout<<"load pcd file successfully!\n"<<endl;
    }*/   //the first version of file load function
    std::vector<PCD, Eigen::aligned_allocator<PCD> > data;
    loadData (argc, argv, data);
    // Check user input
    if (data.empty ())
    {
        PCL_ERROR ("Syntax is: %s <source.pcd> <target.pcd> [*]", argv[0]);
        PCL_ERROR ("[*] - multiple files can be added. The registration results of (i, i+1) will be registered against (i+2), etc");
        return (-1);
    }
    PCL_INFO ("Loaded %d datasets.\n", (int)data.size ());
    PointCloud::Ptr cloud_filtered1 (new PointCloud), cloud_filtered2 (new PointCloud),cloud;

    for(size_t i=0;i<data.size();++i) {
        cloud = data[i].cloud;
        //remove NaN points
        std::vector<int> indices;
        pcl::removeNaNFromPointCloud(*cloud, *cloud, indices);



/*
    std::cerr << "Cloud before filtering: " << std::endl;   //打印
    for (size_t i = 0; i < cloud->points.size (); ++i)
        std::cerr << "    " << cloud->points[i].x << " "
                  << cloud->points[i].y << " "
                  << cloud->points[i].z << std::endl;*/
        /************************************************************************************
         创建直通滤波器的对象，设立参数，滤波字段名被设置为Z轴方向，可接受的范围为（0.0，1.0）
         即将点云中所有点的Z轴坐标不在该范围内的点过滤掉或保留，这里是过滤掉，由函数setFilterLimitsNegative设定
         ***********************************************************************************/
        // 设置滤波器对象
        pcl::PassThrough<pcl::PointXYZ> pass;
        pass.setInputCloud(cloud);            //设置输入点云
        pass.setFilterFieldName("z");         //设置过滤时所需要点云类型的Z字段
        pass.setFilterLimits(0.0, 1.8);        //设置在过滤字段的范围
        //pass.setFilterLimitsNegative (true);   //设置保留范围内还是过滤掉范围内
        pass.filter(*cloud_filtered1);            //执行滤波，保存过滤结果在cloud_filtered
        //std::stringstream sh;
        //sh<<"fileredCloud1"<<".pcd";
        // pcl::io::savePCDFile (sh.str (), *cloud_filtered1, true);
        /************************************************************************************
         *take statistical filer into function
         ************************************************************************************/
        pcl::StatisticalOutlierRemoval<pcl::PointXYZ> sor;
        sor.setInputCloud(cloud_filtered1);
        sor.setMeanK(50);
        sor.setStddevMulThresh(1.5);
        sor.filter(*cloud_filtered2);
/*    std::cerr << "Cloud after filtering: " << std::endl;   //打印
    for (size_t i = 0; i < cloud_filtered->points.size (); ++i)
        std::cerr << "    " << cloud_filtered->points[i].x << " "
                  << cloud_filtered->points[i].y << " "
                  << cloud_filtered->points[i].z << std::endl;*/
        //save the filtered cloud file
        std::stringstream ss;
        ss << "fileredCloud_" << i << ".pcd";
        pcl::io::savePCDFile(ss.str(), *cloud_filtered2, true);
        cout<<"the No."<<i<<"filtered cloud file saved!\n"<<endl;
    }
    return (0);
}